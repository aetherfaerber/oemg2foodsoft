import os
import pandas as pd
import fnmatch
from natsort import natsorted


def oemgbnn2foodsoftcsv(inputfile, angebotart):
    angebot = pd.read_csv(currentweek+"/"+inputfile, ';', encoding='latin1',
                          skiprows=1, usecols=[0, 1, 6, 10, 21, 22, 23, 37],
                          header=None, names=['BestellNR', 'Status', 'Name',
                                              'Produzent', 'Gebinde', 'Anzahl',
                                              'Einheit', 'Preis'],
                          dtype={'BestellNR': str, 'Name': str})
    angebot = angebot.reindex(columns=FoodsoftColumnConvention)
    angebot['Name'] = angebot['Name'].str.replace('"', '')
    angebot['Anzahl'] = angebot['Anzahl'].str.replace(',000', '')
    angebot['MwSt'] = 7
    angebot.to_csv(angebotart+'_'+currentweek+'.csv', ';',
                   encoding='utf-8', quoting=3, header=False, index=False)
    print("Schreibe "+angebotart+"_"+currentweek+".csv")


FoodsoftColumnConvention = ['Status', 'BestellNR', 'Name', 'Notiz',
                            'Produzent', 'Herkunft', 'Einheit', 'Preis',
                            'MwSt', 'Pfand', 'Anzahl', 'Platzhalter1',
                            'Platzhalter2', 'Kategorie']

folders = []
for folder in os.listdir("."):
    if folder.startswith('KW'):
        folders.append(folder)
folders = natsorted(folders)
print(folders)
currentweek = folders[-1]

for filename in fnmatch.filter(os.listdir(currentweek), '*.bnn'):
    if fnmatch.fnmatch(filename, '*Frische*'):
        frischefile = filename
        print("Lese "+currentweek+"/"+frischefile)
    elif fnmatch.fnmatch(filename, '*Basis*'):
        basisfile = filename
        print("Lese "+currentweek+"/"+basisfile)

oemgbnn2foodsoftcsv(frischefile, 'Frischeangebot')
oemgbnn2foodsoftcsv(basisfile, 'Basisangebot')
